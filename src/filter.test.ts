import { filter } from './filter';

test('filter', () => {
  expect(filter(x=>x%2===0, [2, 4, 6, 7])).toEqual([2, 4, 6]);
  expect(filter(x=> x%2===0,[4, 5, 6])).toEqual([4, 6]);
});