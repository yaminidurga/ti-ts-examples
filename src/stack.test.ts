import  Stack  from './stack';
test('Stack', () => {
const x=new Stack();
expect(x.push(10)).toEqual(1);
expect(x.push(11)).toEqual(2);
expect(x.pop()).toEqual(11);
expect(x.pop()).toEqual(10);

expect(x.IsEmpty()).toBeTruthy();
expect(x.Count()).toEqual(0);
})