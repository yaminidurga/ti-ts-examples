import { any,even } from './any';

test('any', () => {
  expect(any(even,[2, 4, 6])).toEqual(true);
  expect(any(even,[1,2,3])).toEqual(true);
  expect(any(even,[1, 3, 5])).toEqual(false);
});
