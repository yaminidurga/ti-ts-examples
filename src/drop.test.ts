import { drop } from './drop';

test('drop', () => {
  expect(drop(2,[1, 2, 3, 4])).toEqual([3, 4]);
  expect(drop(1,[1, 2, 3])).toEqual([2, 3]);
  
});