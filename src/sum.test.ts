import { sum } from './sum';

test('sum', () => {
  expect(sum([1, 2, 3])).toEqual(6);
  expect(sum([4, 5, 6])).toEqual(15);
});