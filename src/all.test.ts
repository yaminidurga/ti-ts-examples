import { all,even } from './all';

test('all', () => {
  expect(all(even,[2, 4, 6])).toEqual(true);
  expect(all(even,[2,4,5])).toEqual(false);
  expect(all(even,[2])).toEqual(true);
  expect(all(even,[1,3,5])).toEqual(false);
});