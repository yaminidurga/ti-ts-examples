import { squareAll } from './squareall';

test('squareAll', () => {
  expect(squareAll([1, 2, 3])).toEqual([1, 4, 9]);
  expect(squareAll([4, 5,])).toEqual([16, 25]);
});