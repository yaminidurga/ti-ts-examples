export function squareAll(arr:number[]):number[]{
  const y=[];
  for(const e of arr){
    y.push(e*e);
  }
  return y;
}