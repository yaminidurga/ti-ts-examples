import { map } from './map';

test('map', () => {
  expect(map(x=>x+2, [2, 4, 6, 7])).toEqual([4, 6, 8, 9]);
  expect(map(x=>x+1, [1, 2, 3])).toEqual([2, 3, 4]);
  });