import { take } from './take';

test('take', () => {
  expect(take(2,[1, 2, 3])).toEqual([1, 2]);
  
});