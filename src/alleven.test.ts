import { alleven } from './alleven';

test('alleven', () => {
  expect(alleven([2, 4, 6])).toEqual([2, 4, 6]);
  expect(alleven([4, 5, 6])).toEqual([4, 6]);
  expect(alleven([1, 3, 4, 6])).toEqual([ 4, 6]);
});
